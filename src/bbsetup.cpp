#include <samy.h>
#include <conio.h>
#include <stdio.h>
#include <keyb.h>
#include <fcntl.h>
#include <io.h>
#include <SYS\STAT.H>

#include <keys60.h>

#define valikko_x 7
#define valikko_y 2
#define max_kohde 6

char kohta=1,key,i,pituus[6]={17,17,17,17,19,11},kohtax[4]={33,57,33,57},kohtay[4]={2,2,10,10};
char loppu=0;
char far *text_mem=(char *far)0xB8000000;
char up[4],left[4],right[4],shoot[4],spec[4];
void do_box(char x,char y,char xx,char yy,char merkki,char col)
{
int i,h;
for(h=x;h<xx;h++)
for(i=y;i<yy;i++)text_mem[(h*2)+(160*i)+1]=col;
for(h=x;h<xx;h++)
for(i=y;i<yy;i++)text_mem[(h*2)+(160*i)]=merkki;
}

void do_rec(char x,char y,char xx,char yy,char col)
{
int i;
text_mem[(x *2)+(160*yy)]=200;
text_mem[(x *2)+(160*yy)+1]=112+col;
text_mem[(x *2)+(160 *y)]=201;
text_mem[(x *2)+(160 *y)+1]=112+col;
text_mem[(xx*2)+(160*yy)]=188;
text_mem[(xx*2)+(160*yy)+1]=112+col;
text_mem[(xx*2)+(160 *y)]=187;
text_mem[(xx*2)+(160 *y)+1]=112+col;
for(i=x+1;i<xx;i++){text_mem[(i*2)+(160 *y)]=205;text_mem[(i*2)+(160 *yy)]=205;text_mem[(i*2)+(160 *y)+1]=112+col;text_mem[(i*2)+(160 *yy)+1]=112+col;}
for(i=y+1;i<yy;i++){text_mem[(x*2)+(160 *i)]=186;text_mem[(xx*2)+(160 *i)]=186;text_mem[(x*2)+(160 *i)+1]=112+col;text_mem[(xx*2)+(160 *i)+1]=112+col;}
}

void do_whole(char x,char y,char xx,char yy,char col_,char merkki,char col)
{
do_box(x+1,y+1,xx+1,yy+1,178,8);
do_box(x  ,y  ,xx  ,yy  ,merkki,col);
do_rec(x  ,y  ,xx-1,yy-1,col_);
}


void do_kohta(char mika)
{
if(mika==5)loppu=2;
else if(mika==6)
		{
		int ti;
		creat("KEYS.BB",S_IREAD|S_IWRITE);
		if((ti=_open("KEYS.BB",O_RDWR))!=-1)
		{
		write(ti,up,sizeof(up));
		write(ti,left,sizeof(left));
		write(ti,right,sizeof(right));
		write(ti,shoot,sizeof(shoot));
		write(ti,spec,sizeof(spec));
		_close(ti);
		loppu=1;
		}
		else
		textcolor(3);
		gotoxy(39,20);
		cprintf("Error in saving keys!       ");
		textcolor(1);
		}
else
	{
	koukuta_i9();
	gotoxy(39,20);
	cprintf("Press a key for Up        ");
	last=0;
	while(last<2);
	up[mika-1]=last;
	gotoxy(39,20);
	cprintf("Press a key for Left      ");
	last=0;
	while(last<2);
	left[mika-1]=last;
	gotoxy(39,20);
	cprintf("Press a key for Right     ");
	last=0;
	while(last<2);
	right[mika-1]=last;
	gotoxy(39,20);
	cprintf("Press a key for Shoot     ");
	last=0;
	while(last<2);
	shoot[mika-1]=last;
	gotoxy(39,20);
	cprintf("Press a key for Special   ");
	last=0;
	while(last<2);
	spec[mika-1]=last;
	gotoxy(39,20);
	cprintf("Keys were read succesfully");
	vapauta_i9();
	for(i=0;i<4;i++)
			{
			textbackground(7);
			textcolor(1);
			gotoxy(kohtax[i]+2,kohtay[i]+2);cprintf("Up     : %-10s",keys[up[i]]);
			gotoxy(kohtax[i]+2,kohtay[i]+3);cprintf("Left   : %-10s",keys[left[i]]);
			gotoxy(kohtax[i]+2,kohtay[i]+4);cprintf("Right  : %-10s",keys[right[i]]);
			gotoxy(kohtax[i]+2,kohtay[i]+5);cprintf("Shoot  : %-10s",keys[shoot[i]]);
			gotoxy(kohtax[i]+2,kohtay[i]+6);cprintf("Special: %-10s",keys[spec[i]]);
			}

	}
}

void main()
{
int ti;
mode_text();
clrscr();

		if((ti=_open("KEYS.BB",O_RDWR))!=-1)
		{
		read(ti,up,sizeof(up));
		read(ti,left,sizeof(left));
		read(ti,right,sizeof(right));
		read(ti,shoot,sizeof(shoot));
		read(ti,spec,sizeof(spec));
		_close(ti);
		}

do_box(0,0,80,1,219,1);
do_box(0,1,80,24,178,5);
do_box(0,24,80,25,219,1);

do_whole(2,2,30,10,1,219,7);

do_whole(2,12,30,22,4,219,7);
/*
do_box( 3, 3,31,23,178,8);
do_box( 2, 2,30,22,219,7);
do_rec( 2, 2,29,21,1);
*/

do_whole(kohtax[0],kohtay[0],kohtax[0]+22,kohtay[0]+7,1,219,7);
do_whole(kohtax[1],kohtay[1],kohtax[1]+22,kohtay[1]+7,1,219,7);
do_whole(kohtax[2],kohtay[2],kohtax[2]+22,kohtay[2]+7,1,219,7);
do_whole(kohtax[3],kohtay[3],kohtax[3]+22,kohtay[3]+7,1,219,7);
do_whole(33,18,78,22,1,219,7);

for(i=0;i<4;i++)
{
textbackground(7);
textcolor(1);
gotoxy(kohtax[i]+2,kohtay[i]+2);cprintf("Up     : %-10s",keys[up[i]]);
gotoxy(kohtax[i]+2,kohtay[i]+3);cprintf("Left   : %-10s",keys[left[i]]);
gotoxy(kohtax[i]+2,kohtay[i]+4);cprintf("Right  : %-10s",keys[right[i]]);
gotoxy(kohtax[i]+2,kohtay[i]+5);cprintf("Shoot  : %-10s",keys[shoot[i]]);
gotoxy(kohtax[i]+2,kohtay[i]+6);cprintf("Special: %-10s",keys[spec[i]]);
}
textcolor(4);
gotoxy(7,15);cprintf("     Warning!");
gotoxy(7,16);cprintf("Don't use arrowkeys, ");
gotoxy(7,17);cprintf("or any else button   ");
gotoxy(7,18);cprintf("that uses two input  ");
gotoxy(7,19);cprintf("code. E.g. Up arrow =");
gotoxy(7,20);cprintf("(Left shift+Up arrow)");


textcolor(15);
textbackground(1);
gotoxy(27,1);
cprintf("Setup for BOOMBOOM 1.00");
gotoxy(65,25);
cprintf("By Samy Smith");
textbackground(7);
textcolor(1);


gotoxy(valikko_x+1,valikko_y+2);cprintf("Set player 1 keys");
gotoxy(valikko_x+1,valikko_y+3);cprintf("Set player 2 keys");
gotoxy(valikko_x+1,valikko_y+4);cprintf("Set player 3 keys");
gotoxy(valikko_x+1,valikko_y+5);cprintf("Set player 4 keys");
gotoxy(valikko_x+1,valikko_y+6);cprintf("Quit without saving");
gotoxy(valikko_x+1,valikko_y+7);cprintf("Save & Quit");

for(i=0;i<pituus[kohta-1];i++)text_mem[(valikko_x+i)*2+(valikko_y+kohta)*160+1]=112-2;
while(loppu==0)
{
key=getch();
if(key==13)do_kohta(kohta);
if(key==27)loppu=2;
if(key==80){for(i=0;i<pituus[kohta-1];i++)text_mem[(valikko_x+i)*2+(valikko_y+kohta)*160+1]=112+1;kohta++;if(kohta>max_kohde)kohta=1;for(i=0;i<pituus[kohta-1];i++)text_mem[(valikko_x+i)*2+(valikko_y+kohta)*160+1]=112-2;}
if(key==72){for(i=0;i<pituus[kohta-1];i++)text_mem[(valikko_x+i)*2+(valikko_y+kohta)*160+1]=112+1;kohta--;if(kohta<1)kohta=max_kohde;for(i=0;i<pituus[kohta-1];i++)text_mem[(valikko_x+i)*2+(valikko_y+kohta)*160+1]=112-2;}
}
textbackground(16);
textcolor(7);
clrscr();
if(key!=27)
{
if(loppu==2)printf("Keys were not saved!\n\rType [BOOMBOOM] to start game!");
else
if(loppu==1)printf("Setup was completed!\n\rType [BOOMBOOM] to start game!");
else
printf("Abnormal Setup quit code!");
}
else printf("User Break!");
}