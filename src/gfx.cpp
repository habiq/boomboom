#include <iostream>
#include <cstdio>
#include <cmath>

#include "gfx.h"
#include "pcx.h"

Gfx::Gfx()
{
    this->fullscreen = false;
}

Gfx::~Gfx()
{
    SDL_FreeSurface(this->screen);
    SDL_FreeSurface(this->font);
    SDL_DestroyWindow(this->window);
    SDL_DestroyRenderer(this->renderer);
    SDL_Quit();
}

bool Gfx::init()
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        this->logError(std::cout, "SDL_Init");
        return false;
    }

    this->window = SDL_CreateWindow("BoomBoom", 100, 100, this->SCREEN_WIDTH, this->SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (this->window == NULL)
    {
        this->logError(std::cout, "CreateWindow");
        return false;
    }

    this->renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (this->renderer == NULL)
    {
        this->logError(std::cout, "CreateRenderer");
        return false;
    }

    this->screen = SDL_CreateRGBSurface(0, 320, 200, 8, 0, 0, 0, 0);

    this->font = SDL_LoadBMP("data/font.bmp");
    SDL_SetColorKey(this->font, SDL_TRUE, SDL_MapRGB(this->font->format, 255, 255, 255));

    return true;
}

void Gfx::toggleFullScreen()
{
    this->fullscreen = !this->fullscreen;
    SDL_SetWindowFullscreen(this->window, this->fullscreen ? SDL_WINDOW_FULLSCREEN : 0);
}

Uint8* Gfx::screenPixels()
{
	return (Uint8*)this->screen->pixels;
}

void Gfx::putText(int x, int y, const char* text, Uint8 color, Uint8 back)
{
    for(int i = 0; text[i] != 0; i++)
    {
        this->putChar(x + i*8, y, text[i], color, back);
    }
}

void Gfx::putChar(int x, int y, const char chr, Uint8 color, Uint8 back)
{
    Uint8 pos = chr - 32;
    int sourceX = (pos % 32) * 8;
    int sourceY = (int)(pos / 32) * 8;
    Uint8* target = (Uint8 *)this->screen->pixels;
    Uint8* font = (Uint8 *)this->font->pixels;
    for(int fy = 0; fy < 8; fy++)
    {
        for(int fx = 0; fx < 8; fx++)
        {
            int fontOffset = sourceX + fx + 256 * (sourceY + fy);
            int ScreenOffset = x + fx + 320 * (fy + y);
            if (!font[fontOffset])
            {
                target[ScreenOffset] = color;
            }
            else if (back != 0)
            {
                target[ScreenOffset] = back;
            }
        }
    }
}

void Gfx::drawLine(int left, int top, int right, int bottom, Uint8 color)
{
    int temp, dx, dy, x, y, x_sign, y_sign, flag;

    dx = std::abs(right - left);
    dy = std::abs(bottom - top);
    if (((dx >= dy) && (left > right)) || ((dy > dx) && (top > bottom)))
    {
        temp = left;
        left = right;
        right = temp;
        temp = top;
        top = bottom;
        bottom = temp;
    }

    if ((bottom - top) < 0)y_sign = -1;
    else y_sign = 1;
    if ((right - left) < 0)x_sign = -1;
    else x_sign = 1;

    if (dx >= dy)
    {
        for (x = left, y = top, flag = 0; x <= right; x++, flag += dy)
        {
            if (flag >= dx)
            {
                flag -= dx;
                y += y_sign;
            }
            this->putPixel(x, y, color);
        }
    }
    else

        for (x = left, y = top, flag = 0; y <= bottom; y++, flag += dx)
        {
            if (flag >= dy)
            {
                flag -= dy;
                x += x_sign;
            }
            this->putPixel(x, y, color);
        }
}


void Gfx::fillRectangle(Uint16 x1, Uint16 y1, Uint16 x2, Uint16 y2, Uint8 col)
{
    SDL_Rect rect;
    rect.x = x1;
    rect.y = y1;
    rect.w = x2 - x1;
    rect.h = y2 - y1;
    SDL_FillRect(this->screen, &rect, col);
}

SDL_Surface* Gfx::loadPCX(const char* filename)
{
	FILE* handle = std::fopen(filename, "rb");
	if (handle == NULL)
	{
		std::cout << "Error opening the file: " << filename << std::endl;
		return NULL;
	}
	PCXHEAD pcx;
	if (std::fread((char *)&pcx, 1, sizeof(PCXHEAD), handle) != sizeof(PCXHEAD))
	{
		std::cout << "Error reading header from: " << filename << std::endl;
		return NULL;
	}

	if (pcx.manufacturer != 10)
	{
		std::cout << "Error validating image file: " << filename << std::endl;
	}
	std::fseek(handle, -769L, SEEK_END);
	Uint8 pcxPalette[768];
	if (std::fgetc(handle) == 12)
	{
		if (std::fread(pcxPalette, 1, 768, handle) != 768)
		{
			std::cout << "Can't read palette from: " << filename << std::endl;
			return NULL;
		}
	}
	int width = pcx.xmax - pcx.xmin + 1;
	int height = pcx.ymax - pcx.ymin + 1;
	SDL_Surface* surface = SDL_CreateRGBSurface(0, width, height, 8, 0, 0, 0, 0);
	SDL_Palette *palette = surface->format->palette;
	for (int i = 0; i < 256; i++)
	{
		SDL_Color *col = &palette->colors[i];
		col->r = pcxPalette[i * 3 + 0];
		col->g = pcxPalette[i * 3 + 1];
		col->b = pcxPalette[i * 3 + 2];
	}

	std::fseek(handle, (unsigned long) sizeof(PCXHEAD), SEEK_SET);
	int bytes = pcx.bytes_per_line;
	Uint8 *pixels = (Uint8 *) surface->pixels;
	for (int i = 0; i < surface->h; ++i)
	{
		if (this->readLinePCX(&pixels[i*surface->w], handle, bytes) != bytes)
		{
			std::cout << "Error reading line from: " << filename << std::endl;
			return false;
		}
	}
	fclose(handle);
	return surface;
}


bool Gfx::renderPCX(const char* filename)
{
	SDL_Surface* pcx = this->loadPCX(filename);
	SDL_SetPaletteColors(this->screen->format->palette, pcx->format->palette->colors, 0, 256);
	SDL_BlitSurface(pcx, NULL, this->screen, NULL);
	SDL_FreeSurface(pcx);
    this->renderScreen();
    return true;
}

void Gfx::clearScreen()
{
	SDL_Rect rect;
	rect.h = screen->h;
	rect.w = screen->w;
	rect.x = 0;
	rect.y = 0;
	SDL_FillRect(this->screen, &rect, 255);
}

void Gfx::renderScreen()
{
    SDL_Texture *tex = SDL_CreateTextureFromSurface(this->renderer, this->screen);
    if (tex == NULL)
    {
        std::cout << "SDL_CreateTextureFromSurface Error: " << SDL_GetError() << std::endl;
        return;
    }

    //First clear the renderer
    SDL_RenderClear(this->renderer);
    //Draw the texture
    SDL_RenderCopy(this->renderer, tex, NULL, NULL);
    //Update the screen
    SDL_RenderPresent(this->renderer);
	SDL_DestroyTexture(tex);
}

void Gfx::verticalLine(int x, int y, int len, Uint8 color)
{
    Uint8 *pixels = (Uint8*)this->screen->pixels;
    memset(pixels + x + (y*this->screen->w), color, len);
}

void Gfx::putPixel(int x, int y, Uint8 color)
{
	Uint8* pixels = (Uint8*)this->screen->pixels;
	pixels[x + y*this->screen->w] = color;
}

void Gfx::setPalette(Uint8 index, Uint8 r, Uint8 g, Uint8 b)
{
    SDL_Color *col = &this->screen->format->palette->colors[index];
    col->r = r << 2;
    col->g = g << 2;
    col->b = b << 2;
}

void Gfx::copyPalette(SDL_Palette* palette)
{
	SDL_Palette *active = this->screen->format->palette;
	for (int i = 0; i < palette->ncolors; i++)
	{
		active->colors[i] = palette->colors[i];
	}
}

void Gfx::clearPalette()
{
	for (int i = 0; i < 256; i++)
	{
		this->setPalette(i, 0, 0, 0);
	}
}

int Gfx::readLinePCX(Uint8 *p, FILE *fp, unsigned int bytes)
{
    unsigned int n = 0, c, i;

    do
    {
        c = fgetc(fp) & 0xff;
        if ((c & 0xc0) == 0xc0)
        {
            i = c & 0x3f;
            c = fgetc(fp);
            while (i--) p[n++] = c;
        }
        else p[n++] = c;
    }
    while (n < bytes);
    return (n);
}

/**
 * Log an SDL error with some error message to the output stream of our choice
 * @param os The output stream to write the message to
 * @param msg The error message to write, format will be msg error: SDL_GetError()
 */
void Gfx::logError(std::ostream &os, const char* msg)
{
    std::cout << msg << " error: " << SDL_GetError() << std::endl;
}
