#ifndef GFX_H
#define	GFX_H

#include <cstdlib>
#include <ostream>
#include <SDL.h>

class Gfx
{
private:
    const int SCREEN_WIDTH  = 640;
    const int SCREEN_HEIGHT = 400;

public:
    Gfx();
    virtual ~Gfx();
    bool init();

    bool renderPCX(const char* filename);
	SDL_Surface* loadPCX(const char* filename);

    void toggleFullScreen();

	Uint8* screenPixels();
    void putText(int x, int y, const char* text, Uint8 col, Uint8 back);
    void putChar(int x, int y, const char chr, Uint8 color, Uint8 back);
	void clearScreen();
	void renderScreen();

    void drawLine(int left, int top, int right, int bottom, Uint8 color);
	void verticalLine(int x, int y, int len, Uint8 color);
	void putPixel(int x, int y, Uint8 color);

    void setPalette(Uint8 index, Uint8 r, Uint8 g, Uint8 b);
	void copyPalette(SDL_Palette* palette);
	void clearPalette();
    void fillRectangle(Uint16 x1, Uint16 y1, Uint16 x2, Uint16 y2, Uint8 col);

private:

    int readLinePCX(Uint8 *p, FILE *fp, unsigned int bytes);
    void logError(std::ostream &os, const char* msg);

private:
    SDL_Window *window;
    SDL_Renderer *renderer;
    SDL_Surface *screen;
    SDL_Surface *font;

    bool fullscreen;
};

#endif	/* GFX_H */

