#include <cstdlib>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <ctime>

#include "gfx.h"
#include "pcx.h"

#define MAX_SMOKE 210
#define MAX_AMMO 700
#define MAX_SPECIAL 150
#define MAX_PLAYERS 5
#define MAX_STAGES 5
#define MAX_ENERGY 150
#define DEFAULT_PLAYER_COUNT 2

#define debugTrace SDL_Log

// Graphics
Gfx* gfx;
Uint8 *backbuffer;
Uint8 *kuva_ala;
Uint8 *kuva_yla;

int xkord[4] = {3, 165, 3, 165},
ykord[4] = {3, 103, 103, 3},
xleng[4] = {150, 150, 150, 150},
yleng[4] = {90, 90, 90, 90};


int lp = 1; //Virkistys
int run_in_click = 7; //Framerate

int osuma[MAX_PLAYERS];

double dcos[361],
dsin[361],
ax[MAX_PLAYERS],
ay[MAX_PLAYERS],
px[MAX_AMMO],
py[MAX_AMMO],
psx[MAX_AMMO],
psy[MAX_AMMO],
asx[MAX_PLAYERS],
asy[MAX_PLAYERS],
pex[MAX_SPECIAL],
pey[MAX_SPECIAL],
ex[MAX_SPECIAL],
ey[MAX_SPECIAL],
savx[MAX_SMOKE],
savy[MAX_SMOKE];


unsigned char special[MAX_PLAYERS], 
cheat = 0,
stages[MAX_STAGES] = { 1, 1, 1, 0, 0 },
laskuri, 
tilanne[MAX_PLAYERS],
type[MAX_SPECIAL], 
pelaajat = DEFAULT_PLAYER_COUNT;
        
char viesti[MAX_PLAYERS][19];
char aseet[15][19] = { 
    "Multishot", 
    "Teleport", 
    "Repairer", 
    "Shotgun", 
    "Rshotgun", 
    "Invisibility", 
    "Diggers", 
    "Heat Hunters", 
    "Punisher", 
    "Missile", 
    "Bomb", 
    "Sprinkler", 
    "Shaker", 
    "JUDGER!", 
    "f" 
};
unsigned int xkohta[MAX_PLAYERS], 
ykohta[MAX_PLAYERS], 
shootRepeatBlock[5], 
click, 
load[MAX_PLAYERS],
klo, 
pituus[MAX_PLAYERS];

unsigned char up[4] = {SDL_SCANCODE_W, SDL_SCANCODE_KP_5, SDL_SCANCODE_P, SDL_SCANCODE_Y },
leftKey[4] = {SDL_SCANCODE_A, SDL_SCANCODE_KP_1, SDL_SCANCODE_L, SDL_SCANCODE_G },
rightKey[4] = {SDL_SCANCODE_D, SDL_SCANCODE_KP_3, SDL_SCANCODE_APOSTROPHE, SDL_SCANCODE_J },
shoot[4] = {SDL_SCANCODE_GRAVE, SDL_SCANCODE_KP_4, SDL_SCANCODE_O, SDL_SCANCODE_T },
spec[4] = {SDL_SCANCODE_S, SDL_SCANCODE_KP_2, SDL_SCANCODE_SEMICOLON, SDL_SCANCODE_H };

int ener[MAX_PLAYERS] = {1, 1, 1, 1}, 
rep_speed[MAX_PLAYERS], 
angle[MAX_PLAYERS] = {0, 0}, 
real_angr[MAX_PLAYERS], 
kesto,
real_angl[MAX_PLAYERS], 
sav[MAX_SMOKE], 
savang[MAX_SMOKE], 
bensa[MAX_PLAYERS],
raj_x[40], 
raj_y[40], 
raj_time[40];

std::vector<std::string> stageNames;

const unsigned char rajahdys[12][12] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 216, 0, 0, 0, 216, 216, 0, 0, 0,
    0, 0, 0, 215, 215, 215, 215, 215, 215, 0, 0, 0,
    0, 216, 216, 215, 215, 216, 216, 216, 0, 0, 216, 0,
    0, 216, 216, 215, 216, 216, 217, 216, 216, 216, 216, 0,
    216, 216, 217, 216, 216, 217, 217, 216, 217, 217, 216, 216,
    216, 215, 216, 217, 215, 217, 217, 216, 216, 217, 216, 216,
    0, 216, 216, 216, 216, 216, 217, 216, 216, 216, 216, 0,
    0, 216, 215, 216, 216, 216, 216, 216, 217, 217, 216, 0,
    0, 0, 216, 215, 216, 216, 215, 215, 215, 217, 0, 0,
    0, 0, 0, 216, 216, 216, 215, 0, 0, 217, 216, 0,
    0, 0, 0, 0, 0, 216, 0, 0, 0, 0, 215, 0};


double sindeg(double R)
{
    return (sin((float)(3.14 * R / 180)));
}

double cosdeg(double R)
{
    return (cos((float)(3.14 * R / 180)));
}

unsigned int ticks()
{
    return SDL_GetTicks() >> 6;
}

double bigger(double st, double nd)
{
    if (st > nd)return st;
    else return nd;
}

int stageCount()
{
    return stageNames.size();
}

const std::string stageNameFile(int index)
{
    return std::string("stages/") + stageNames[index - 1];
}

const std::string stageNamePlain(int index)
{
    std::string tmp = stageNames[index - 1];
    for (int h = 0; h < tmp.length(); h++)
    {
        if (tmp[h] == '.')
        {
            tmp.erase(h);
            break;
        }
    }
    return tmp;
}

void handle_global_events(SDL_Event& event)
{
    switch (event.type)
    {
    case SDL_QUIT:
        delete gfx;
        exit(0);
        break;

    case SDL_WINDOWEVENT:
        debugTrace("WindowEvent [0x%x, d%d]", event.window.event, event.window.event);
        switch (event.window.event)
        {
        case SDL_WINDOWEVENT_EXPOSED:
            gfx->renderScreen();
            break;
        }
        break;

    case SDL_KEYDOWN:
        if (event.key.keysym.scancode == SDL_SCANCODE_F12)
        {
            gfx->toggleFullScreen();
        }
        break;

    case SDL_KEYUP:
    case SDL_MOUSEMOTION:
        break;

    default:
        debugTrace("Event [0x%x, d%d]", event.type, event.type);
        break;
    }
}

unsigned int waitForKey()
{
    SDL_Event event;
    while (true)
    {
        if (SDL_PollEvent(&event) == 1)
        {
            handle_global_events(event);
            switch (event.type)
            {
                case SDL_KEYDOWN:
                    return event.key.keysym.scancode;
            }
        }
        else
        {
            SDL_Delay(100);
        }
    }

}

void piirra_energiat()
{
    for (int i = 0; i < pelaajat; i++)
    {
        if (ener[i] > 1)
        {
            gfx->verticalLine(xkord[i], ykord[i] + yleng[i] + 3, (int)((float)(ener[i] / (float)MAX_ENERGY) * 150), 254 - i);
        }
        if (ener[i] < MAX_ENERGY - 1)
        {
            gfx->verticalLine(xkord[i] + (int)((float)(ener[i] / (float)MAX_ENERGY) * 150), ykord[i] + yleng[i] + 3, 150 - (int)((float)(ener[i] / (float)MAX_ENERGY) * 150), 255);
        }
    }
}

void piirra_bensa()
{
    for (int i = 0; i < pelaajat; i++)
    {
        if (bensa[i] > 87)gfx->drawLine(xkord[i], ykord[i] + yleng[i] + 5, xkord[i] + bensa[i] / 86, ykord[i] + yleng[i] + 5, 218);
        gfx->drawLine(xkord[i] + bensa[i] / 86, ykord[i] + yleng[i] + 5, xkord[i] + 150, ykord[i] + yleng[i] + 5, 255);
    }
}

void toinen_panos(int x, int y, int ang, double speed, unsigned char typ, short kuka)
{
    int i = 0;
    while (ex[i] != 0 && i < MAX_SPECIAL)i++;
    if (kuka >= 0 && kuka < 4)
    {
        asx[kuka] += dcos[(ang + 180) % 360] / 3;
        asy[kuka] += dsin[(ang + 180) % 360] / 3;
    }
    pex[i] = dcos[ang] * speed;
    pey[i] = dsin[ang] * speed;
    ex[i] = x;
    ey[i] = y;
    type[i] = typ;
}

void tee_panos(int x, int y, short player, int ang, double speed)
{
    int j = 0;
    while (ang > 360)ang -= 360;
    while (ang < 0)ang += 360;
    while (px[j] > 0 && j < MAX_AMMO)j++;
    if (player >= 0 && player < 4)
    {
        psx[j] = dcos[(ang) % 360] + asx[player] / 2;
        psy[j] = dsin[(ang) % 360] + asy[player] / 2;
    }
    else
    {
        psx[j] = dcos[ang % 360] * speed;
        psy[j] = dsin[ang % 360] * speed;
    }
    px[j] = x + 3 * psx[j];
    py[j] = y + 3 * psy[j];
}

void tee_rajays(int x, int y, int teho, int code)
{
    signed int t, w, e, angle;
    double speed;
    t = 0;
    while (raj_time[t] != 0 && t < 40)t++;
    raj_time[t] = 10;
    raj_x[t] = x;
    raj_y[t] = y;
    for (w = -6; w < 6; w++)
        for (e = -6; e < 6; e++)
        {
            if (rajahdys[e + 6][w + 6] != 0)
            {
                if (y + w < 200)
                {
                    if (y + w > 1 && x + e > 1 && x + e < 319)
                        if ((unsigned char) kuva_yla [x + e + (y + w)*320] <= 196)
                            kuva_yla [x + e + 320 * (y + w)] = rajahdys[e + 6][w + 6];
                }
                else
                    if (y + w < 399 && x + e > 1 && x + e < 319)
                    if ((unsigned char) kuva_ala[x + e + (y + w - 200)*320] <= 196)
                        kuva_ala[x + e + 320 * (y + w - 200)] = rajahdys[e + 6][w + 6];
            }
        }
    for (int d = 0; d < teho; d++)
    {
        angle = rand() % 360;
        speed = rand() % 50;
        speed = float(speed / 100 + .1);
        if (code == 1)tee_panos(x, y, 4, angle, 1);
        if (code == 2)tee_panos(x, y, 4, angle, speed);
    }

}

void liikuta_panos(int i, char draw)
{
    if ((int) (py[i]) < 200)
        if ((unsigned char) (kuva_yla[(long) (px[i])+(long) (py[i])*320]) == 0 || (unsigned char) (kuva_yla[(long) (px[i])+(long) (py[i])*320]) > 196)
        {
            px[i] += psx[i];
            py[i] += psy[i];
        }
        else
        {
            kuva_yla[(long) (px[i])+(long) (py[i])*320] = 0;
            px[i] = 0;
        }

    if ((int) (py[i]) >= 200)
        if ((unsigned char) (kuva_ala[(long) (px[i])+(long) (py[i] - 200)*320]) == 0 || (unsigned char) (kuva_ala[(long) (px[i])+(long) (py[i] - 200)*320]) > 196)
        {
            px[i] += psx[i];
            py[i] += psy[i];
        }
        else
        {
            kuva_ala[(long) (px[i])+(long) (py[i] - 200)*320] = 0;
            px[i] = 0;
        }

    for (int h = 0; h < pelaajat; h++)
    {
        if (abs((int) (px[i] - ax[h])) < 2 && std::abs((int) (py[i] - ay[h])) < 2 && ener[h] > 0)
        {
            osuma[h] += 10;
            asy[h] += psy[i] / 7;
            asx[h] += psx[i] / 7;
            ener[h] -= 4;
            px[i] = 0;
            piirra_energiat();
        }
    }

    if (py[i] > 398 || py[i] < 2 || px[i] < 2 || px[i] > 318)
    {
        px[i] = 0;
    }
    else
    {
        if (draw == lp - 1)
            for (int h = 0; h < pelaajat; h++)
            {
                if (px[i] > xkohta[h] && px[i] < xkohta[h] + xleng[h] && py[i] > ykohta[h] && py[i] < ykohta[h] + yleng[h])
                    backbuffer[xkord[h]+(int) (px[i] - xkohta[h])+(int) (ykord[h] + py[i] - ykohta[h])*320] = 250;
            }
    }
}

void liikuta_savu(short i, char draw)
{
    if ((int) (savy[i]) < 200)
        if ((unsigned char) (kuva_yla[(long) (savx[i])+(long) (savy[i])*320]) == 0 || (unsigned char) (kuva_yla[(long) (savx[i])+(long) (savy[i])*320]) >= 197)
        {
            savx[i] += dcos[savang[i]];
            savy[i] += dsin[savang[i]];
        }

    if ((int) (savy[i]) >= 200)
        if ((unsigned char) (kuva_ala[(long) (savx[i])+(long) (savy[i] - 200)*320]) == 0 || (unsigned char) (kuva_ala[(long) (savx[i])+(long) (savy[i] - 200)*320]) >= 197)
        {
            savx[i] += dcos[savang[i]];
            savy[i] += dsin[savang[i]];
        }

    if (savy[i] > 399 || savy[i] < 1 || savx[i] < 1 || savx[i] > 319)sav[i] = 0;
    else
    {
        if (draw == lp - 1)
            for (int h = 0; h < pelaajat; h++)
            {
                if (savx[i] > xkohta[h] && savx[i] < xkohta[h] + xleng[h] && savy[i] > ykohta[h] && savy[i] < ykohta[h] + yleng[h])
                    backbuffer[xkord[h]+(int) (savx[i] - xkohta[h])+(int) (ykord[h] + savy[i] - ykohta[h])*320] = 212;
            }
    }
    sav[i]--;
    if (sav[i] < 0)sav[i] = 0;
}

void liikuta_erikoinen(short i, char draw)
{
    char nrst, tmp;
    double nrn;
    double nearx, neary;
    if (type[i] >= 9 && type[i] <= 12)
    {
        nrn = 1000;
        for (tmp = 0; tmp < pelaajat; tmp++)
        {
            if (ener[tmp] > 0 && tmp + 9 != type[i] && bigger(std::abs(ax[tmp] - ex[i]), std::abs(ay[tmp] - ey[i])) < nrn)
            {
                nrst = tmp;
                nrn = bigger(std::abs(ax[tmp] - ex[i]), std::abs(ay[tmp] - ey[i]));
            }
        }
        if (nrn != 1000)
        {
            nearx = std::abs(ax[nrst] - ex[i]);
            neary = std::abs(ay[nrst] - ey[i]);
            if (std::abs(nearx) > std::abs(neary))
            {
                if (nearx != 0)neary /= nearx;
                nearx = 1;
            }
            else
            {
                if (nearx != 0)nearx /= neary;
                neary = 1;
            }
            if (ax[nrst] < ex[i] && ay[nrst] < ey[i])
            {
                pex[i] = (pex[i]-(nearx / 70)) / 1.03;
                pey[i] = (pey[i]-(neary / 70)) / 1.03;
            }
            if (ax[nrst] < ex[i] && ay[nrst] > ey[i])
            {
                pex[i] = (pex[i]-(nearx / 70)) / 1.03;
                pey[i] = (pey[i]+(neary / 70)) / 1.03;
            }
            if (ax[nrst] > ex[i] && ay[nrst] < ey[i])
            {
                pex[i] = (pex[i]+(nearx / 70)) / 1.03;
                pey[i] = (pey[i]-(neary / 70)) / 1.03;
            }
            if (ax[nrst] > ex[i] && ay[nrst] > ey[i])
            {
                pex[i] = (pex[i]+(nearx / 70)) / 1.03;
                pey[i] = (pey[i]+(neary / 70)) / 1.03;
            }
        }
        if (pex[i] > 1)pex[i] = 1;
        if (pey[i] > 1)pey[i] = 1;
        if (pey[i]<-1)pey[i] = -1;
        if (pex[i]<-1)pex[i] = -1;

    }

    if ((int) (ey[i]) < 200)
        if (tmp = (unsigned char) (kuva_yla[(long) (ex[i])+(long) (ey[i])*320]) == 0 || (unsigned char) (kuva_yla[(long) (ex[i])+(long) (ey[i])*320]) >= 197)
        {
            ex[i] += pex[i];
            ey[i] += pey[i];
            if (tmp == 197 && type[i] >= 9)ex[i] = 0;
            //Vaikutukset lennossa
            if (type[i] == 7 && rand() % 3 == 0)
            {
                tee_panos(ex[i], ey[i], 5, rand() % 360, rand() % 15 / 20 + 0.1);
                tee_panos(ex[i], ey[i], 5, rand() % 360, rand() % 15 / 20 + 0.1);
            }
            if (type[i] == 13)
            {
                tee_panos(ex[i], ey[i], 5, rand() % 360, rand() % 15 / 20 + 0.2);
                tee_panos(ex[i], ey[i], 5, rand() % 360, rand() % 15 / 20 + 0.2);
            }
        }
        else
        {
            if (type[i] != 8)kuva_yla[(long) (ex[i])+(long) (ey[i])*320] = 0;
            if (type[i] == 1 && rand() % 50 > 0)
            {
                kuva_yla[(long) (ex[i] + 1)+(long) (ey[i])*320] = 0;
                kuva_yla[(long) (ex[i] - 1)+(long) (ey[i])*320] = 0;
                kuva_yla[(long) (ex[i])+(long) (ey[i] - 1)*320] = 0;
                kuva_yla[(long) (ex[i])+(long) (ey[i] + 1)*320] = 0;
                toinen_panos(ex[i], ey[i], rand() % 360, rand() % 20 / 10, 1, 4);
                tee_rajays(ex[i], ey[i], 5, 1);
            }
            if (type[i] == 8 && rand() % 100 != 0)toinen_panos(ex[i], ey[i], rand() % 360, rand() % 15 / 10 + 0.1, 8, 4);
            if (type[i] == 5)tee_rajays(ex[i], ey[i], 40, 1);
            if (type[i] >= 9 &&
                type[i] <= 12)tee_rajays(ex[i], ey[i], 20, 2);
            if (type[i] == 3)tee_rajays(ex[i], ey[i], 10, 2);
            if (type[i] == 6)tee_rajays(ex[i], ey[i], 110, 2);
            if (type[i] == 13)for (tmp = 0; tmp < 7; tmp++)
                {
                    tee_rajays(ex[i] + rand() % 26 - 13, ey[i] + rand() % 26 - 13, 60, 2);
                }
            ex[i] = 0;
        }

    if ((int) (ey[i]) >= 200)
        if (tmp = (unsigned char) (kuva_ala[(long) (ex[i])+(long) (ey[i] - 200)*320]) == 0 || (unsigned char) (kuva_ala[(long) (ex[i])+(long) (ey[i] - 200)*320]) >= 197)
        {
            ex[i] += pex[i];
            ey[i] += pey[i];
            if (tmp == 197 && type[i] >= 9)ex[i] = 0;
            //Vaikutukset lennossa
            if (type[i] == 7 && rand() % 3 == 0)
            {
                tee_panos(ex[i], ey[i], 5, rand() % 360, rand() % 15 / 20 + 0.1);
                tee_panos(ex[i], ey[i], 5, rand() % 360, rand() % 15 / 20 + 0.1);
            }
            if (type[i] == 13)
            {
                tee_panos(ex[i], ey[i], 5, rand() % 360, rand() % 15 / 20 + 0.2);
                tee_panos(ex[i], ey[i], 5, rand() % 360, rand() % 15 / 20 + 0.2);
            }
        }
        else
        {
            if (type[i] != 8)kuva_ala[(long) (ex[i])+(long) (ey[i] - 200)*320] = 0;
            if (type[i] == 1 && rand() % 50 > 0)
            {
                kuva_ala[(long) (ex[i] + 1)+(long) (ey[i] - 200)*320] = 0;
                kuva_ala[(long) (ex[i] - 1)+(long) (ey[i] - 200)*320] = 0;
                kuva_ala[(long) (ex[i])+(long) (ey[i] - 201)*320] = 0;
                kuva_ala[(long) (ex[i])+(long) (ey[i] - 199)*320] = 0;
                toinen_panos(ex[i], ey[i], rand() % 360, rand() % 20 / 10, 1, 4);
                tee_rajays(ex[i], ey[i], 5, 1);
            }
            if (type[i] == 8 && rand() % 100 != 0)toinen_panos(ex[i], ey[i], rand() % 360, rand() % 15 / 10 + 0.1, 8, 4);
            if (type[i] == 5)tee_rajays(ex[i], ey[i], 40, 1);
            if (type[i] >= 9 &&
                type[i] <= 12)tee_rajays(ex[i], ey[i], 20, 2);
            if (type[i] == 3)tee_rajays(ex[i], ey[i], 10, 2);
            if (type[i] == 6)tee_rajays(ex[i], ey[i], 110, 2);
            if (type[i] == 13)for (tmp = 0; tmp < 7; tmp++)
                {
                    tee_rajays(ex[i] + rand() % 26 - 13, ey[i] + rand() % 26 - 13, 60, 2);
                }
            ex[i] = 0;
        }

    for (int h = 0; h < pelaajat; h++)
        if (abs((int) (ex[i] - ax[h])) < 2 && std::abs((int) (ey[i] - ay[h])) < 2 && ener[h] > 0)
        {
            osuma[h] += 10;
            //vaikutukset
            if (type[i] == 4)
            {
                asy[h] += pey[i]*3;
                asx[h] += pex[i]*3;
            }
            if (type[i] == 5)tee_rajays(ex[i], ey[i], 40, 1);
            if (type[i] >= 9 &&
                type[i] <= 12)tee_rajays(ex[i], ey[i], 20, 2);
            if (type[i] == 3)
            {
                tee_rajays(ex[i], ey[i], 10, 2);
            }
            if (type[i] == 6)
            {
                tee_rajays(ex[i], ey[i], 110, 2);
            }
            if (type[i] == 13)for (tmp = 0; tmp < 7; tmp++)
                {
                    tee_rajays(ex[i] + rand() % 26 - 13, ey[i] + rand() % 26 - 13, 60, 2);
                }
            if (type[i] == 8)
            {
                osuma[h] += 1200;
            }
            //loppu

            asy[h] += pey[i] / 7;
            asx[h] += pex[i] / 7;
            ener[h] -= 4;
            if (type[i] == 1)ener[h] -= 2;
            piirra_energiat();
            ex[i] = 0;
        }
    if (ey[i] > 399 || ey[i] < 1 || ex[i] < 1 || ex[i] > 319)ex[i] = 0;
    else
        if (draw == lp - 1)
        for (int h = 0; h < pelaajat; h++)
        {
            if (ex[i] > xkohta[h] && ex[i] < xkohta[h] + xleng[h] && ey[i] > ykohta[h] && ey[i] < ykohta[h] + yleng[h])
            {
                backbuffer[xkord[h]+ (int) (ex[i] - xkohta[h])+(int) (ykord[h] + ey[i] - ykohta[h])*320] = 250 - type[i];
                backbuffer[xkord[h] + 1 + (int) (ex[i] - xkohta[h])+(int) (ykord[h] + ey[i] - ykohta[h])*320] = 250 - type[i];
                backbuffer[xkord[h]+ (int) (ex[i] - xkohta[h])+(int) (ykord[h] + ey[i] - ykohta[h] + 1)*320] = 250 - type[i];
                backbuffer[xkord[h] - 1 + (int) (ex[i] - xkohta[h])+(int) (ykord[h] + ey[i] - ykohta[h])*320] = 250 - type[i];
                backbuffer[xkord[h]+ (int) (ex[i] - xkohta[h])+(int) (ykord[h] + ey[i] - ykohta[h] - 1)*320] = 250 - type[i];
            }
        }
}


void put_only_text(int x, int y, const char* text, int color)
{
    gfx->putText(x, y, text, color, 0);
}

void put_only_letter(int x, int y, char letter, int color)
{
    gfx->putChar(x, y, letter, color, 0); 
}

void set_player_text(int index, const char *text, int length)
{
    strcpy(viesti[index], text);
    pituus[index] = length;
}

void draw_stages()
{
    gfx->fillRectangle(117, 67, 195, 122, 206);
    for (int i = 0; i < 5; i++)
    {
        int j = 1;
        if (stages[i] != 0)
        {
            std::string filename = stageNamePlain(stages[i]);
            gfx->putText(128, 72 + 10 * i, filename.c_str(), 240 + i, 206);
        }
        else
        {
            gfx->putText(128, 72 + 10 * i, " -", 240 + i, 206);
        }
    }
    gfx->renderScreen();
}

void select_stage()
{
    int kohta = 0;
    gfx->setPalette(240, 1, 30, 30);
    gfx->setPalette(241, 1, 30, 30);
    gfx->setPalette(242, 1, 30, 30);
    gfx->setPalette(243, 1, 30, 30);
    gfx->setPalette(244, 1, 30, 30);

    gfx->setPalette(kohta + 240, 63, 1, 1);
    draw_stages();
    int last = 0;
    while (last != SDL_SCANCODE_ESCAPE)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            handle_global_events(event);
            switch (event.type)
            {
            case SDL_KEYDOWN:
                last = event.key.keysym.scancode;
                if (event.key.keysym.scancode == SDL_SCANCODE_R)
                {
                    stages[kohta] = rand() % stageCount() + 1;
                    draw_stages();
                }
                if (event.key.keysym.scancode == SDL_SCANCODE_UP)
                {
                    if (kohta > 0)
                    {
                        gfx->setPalette(kohta + 240, 1, 30, 30);
                        kohta--;
                        gfx->setPalette(kohta + 240, 63, 1, 1);
                        gfx->renderScreen();
                    }
                }
                if (event.key.keysym.scancode == SDL_SCANCODE_DOWN)
                {
                    if (kohta < 4 && stages[kohta] != 0)
                    {
                        gfx->setPalette(kohta + 240, 1, 30, 30);
                        kohta++;
                        gfx->setPalette(kohta + 240, 63, 1, 1);
                        gfx->renderScreen();
                    }
                }
                if (event.key.keysym.scancode == SDL_SCANCODE_LEFT)
                {
                    if (stages[kohta] > 0)
                    {
                        stages[kohta]--;
                        if (stages[kohta] == 0)for (int g = kohta + 1; g < 5; g++)stages[g] = 0;
                        if (stages[0] == 0)stages[0] = 1;
                        draw_stages();
                    }
                }
                if (event.key.keysym.scancode == SDL_SCANCODE_RIGHT)
                {
                    if (stages[kohta] < stageCount())
                    {
                        stages[kohta]++;
                        draw_stages();
                    }
                }

                if (event.key.keysym.scancode == SDL_SCANCODE_1)
                {
                    for (int g = 0; g < 5; g++)
                    {
                        if (g < 1)stages[g] = rand() % stageCount() + 1;
                        else stages[g] = 0;
                    }
                    gfx->setPalette(kohta + 240, 1, 30, 30);
                    gfx->setPalette(240, 63, 1, 1);
                    draw_stages();
                    kohta = 0;
                }
                if (event.key.keysym.scancode == SDL_SCANCODE_2)
                {
                    for (int g = 0; g < 5; g++)
                    {
                        if (g < 2)stages[g] = rand() % stageCount() + 1;
                        else stages[g] = 0;
                    }
                    gfx->setPalette(kohta + 240, 1, 30, 30);
                    gfx->setPalette(240, 63, 1, 1);
                    draw_stages();
                    kohta = 0;
                }
                if (event.key.keysym.scancode == SDL_SCANCODE_3)
                {
                    for (int g = 0; g < 5; g++)
                    {
                        if (g < 3)stages[g] = rand() % stageCount() + 1;
                        else stages[g] = 0;
                    }
                    gfx->setPalette(kohta + 240, 1, 30, 30);
                    gfx->setPalette(240, 63, 1, 1);
                    draw_stages();
                    kohta = 0;
                }
                if (event.key.keysym.scancode == SDL_SCANCODE_4)
                {
                    for (int g = 0; g < 5; g++)
                    {
                        if (g < 4)stages[g] = rand() % stageCount() + 1;
                        else stages[g] = 0;
                    }
                    gfx->setPalette(kohta + 240, 1, 30, 30);
                    gfx->setPalette(240, 63, 1, 1);
                    draw_stages();
                    kohta = 0;
                }
                if (event.key.keysym.scancode == SDL_SCANCODE_5)
                {
                    for (int g = 0; g < 5; g++)
                    {
                        if (g < 5)stages[g] = rand() % stageCount() + 1;
                        else stages[g] = 0;
                    }
                    gfx->setPalette(kohta + 240, 1, 30, 30);
                    gfx->setPalette(240, 63, 1, 1);
                    draw_stages();
                    kohta = 0;
                }
                break;
            }
        }
        SDL_Delay(50);
    }
}

void options()
{
    int kohta = 0;
    gfx->setPalette(240, 63, 1, 1);
    gfx->setPalette(241, 1, 30, 30);
    gfx->setPalette(242, 1, 30, 30);
    while (1)
    {
        gfx->fillRectangle(117, 67, 195, 122, 206);
        gfx->putText(115, 68, "Players", 240, 206);
        gfx->putChar(185, 68, pelaajat + 48, 240, 206);

        gfx->putText(115, 78, "Refresh 1/", 241, 206);
        gfx->putChar(198, 78, lp + 48, 241, 206);

        gfx->putText(115, 88, "Speed", 242, 206);
        gfx->putChar(185, 88, 48 + run_in_click, 242, 206);
        gfx->renderScreen();

        int last = 0;
        while (last != SDL_SCANCODE_RETURN)
        {
            SDL_Event event;
            while (SDL_PollEvent(&event))
            {
                handle_global_events(event);
                switch (event.type)
                {
                case SDL_KEYDOWN:
                    last = event.key.keysym.scancode;
                    if (event.key.keysym.scancode == SDL_SCANCODE_UP)
                    {
                        gfx->setPalette(kohta + 240, 1, 30, 30);
                        if (kohta > 0)kohta--;
                        gfx->setPalette(kohta + 240, 63, 1, 1);
                        gfx->renderScreen();
                    }
                    if (event.key.keysym.scancode == SDL_SCANCODE_DOWN)
                    {
                        gfx->setPalette(kohta + 240, 1, 30, 30);
                        if (kohta < 2)kohta++;
                        gfx->setPalette(kohta + 240, 63, 1, 1);
                        gfx->renderScreen();
                    }
                    if (event.key.keysym.scancode == SDL_SCANCODE_RETURN)
                    {
                        if (kohta == 0)
                        {
                            pelaajat++;
                            if (pelaajat > 4)pelaajat = 1;
                        }
                        if (kohta == 1)
                        {
                            lp++;
                            if (lp > 5)lp = 1;
                        }
                        if (kohta == 2)
                        {
                            run_in_click++;
                            if (run_in_click > 8)run_in_click = 3;
                        }
                    }
                    if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
                    {
                        gfx->fillRectangle(117, 67, 206, 122, 206);
                        return;
                    }
                    break;
                }
            }
            SDL_Delay(50);
        }
    }
}

void tee_savu(int player, int ang, char rd, char pituus)
{
    int i = 0;
    while (sav[i] > 0 && i < MAX_SMOKE)i++;
    savang[i] = (ang + 180 + rand() % rd - rd / 2) % 360;
    savx[i] = ax[player];
    savy[i] = ay[player];
    savx[i] += 2 * dcos[savang[i]];
    savy[i] += 2 * dsin[savang[i]];
    sav[i] = rand() % pituus + pituus;
}

void clear_data()
{
    for (int i = 0; i < MAX_AMMO; i++)px[i] = 0;
    for (int i = 0; i < MAX_SPECIAL; i++)ex[i] = 0;
    for (int i = 0; i < MAX_SMOKE; i++)sav[i] = 0;
    for (int i = 0; i < 20; i++)raj_time[i] = 0;

    for (int i = 0; i < pelaajat; i++)
    {
        osuma[i] = 0;
        asx[i] = 0;
        asy[i] = 0;
        angle[i] = 270;
        ener[i] = MAX_ENERGY;
    }
}

void error(const char *s)
{
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
        "Error",
        s,
        NULL);
    exit(-1);
}

void erikoinen(short player, short weapon)
{
    char a;
    if (weapon == 0)
    {
        load[player] = 290;
        for (a = 0; a < 91; a++)tee_panos(ax[player], ay[player], 4, 4 * a, 1.5);
    }
    if (weapon == 1)
    {
        load[player] = 1000;
        asy[player] = 0;
        asx[player] = 0;
        int i = 0;
        do
        {
            ax[player] = rand() % 310 + 5;
            ay[player] = rand() % 390 + 5;
            if (ay[player] >= 200)
            {
                if ((unsigned char) kuva_ala[(int) (ax[player])+(int) (ay[player] - 200)*320] != 0 && (unsigned char) kuva_ala[(int) (ax[player])+(int) (ay[player] - 200)*320] < 197)i = 1;
            }
            else if ((unsigned char) kuva_yla[(int) (ax[player])+(int) (ay[player])*320] != 0 && (unsigned char) kuva_yla[(int) (ax[player])+(int) (ay[player])*320] < 197)i = 1;
            if (i == 0 && ax[player] < xleng[player])xkohta[player] = 1;
            if (i == 0 && ay[player] < yleng[player])ykohta[player] = 1;
            if (i == 0 && ax[player] > 319 - xleng[player])xkohta[player] = 319 - xleng[player];
            if (i == 0 && ay[player] > 399 - yleng[player])ykohta[player] = 399 - yleng[player];
        }
        while (i == 1);
    }
    // 2 parannus
    if (weapon == 3)
    {
        load[player] = 300;
        for (a = 1; a < 20; a++)tee_panos(ax[player] - asy[player], ay[player] - asy[player], 5, (angle[player] + rand() % 30 - 15) % 360, 1.6 + rand() % 5 / 5);
    }
    if (weapon == 4)
    {
        load[player] = 200;
        for (a = 1; a < 20; a++)tee_panos(ax[player], ay[player], player, angle[player] + 170 + a, 0);
    }
    // 5 näkymätön
    if (weapon == 6)
    {
        load[player] = 200;
        toinen_panos(ax[player] + dcos[angle[player]]*3, ay[player] + dsin[angle[player]]*3, angle[player], 1.7, 1, player);
    }
    if (weapon == 7)
    {
        load[player] = 600;
        toinen_panos(ax[player] + dcos[angle[player]]*3, ay[player] + dsin[angle[player]]*3, angle[player], 2.1, player + 9, player);
    }
    if (weapon == 8)
    {
        load[player] = 70;
        toinen_panos(ax[player] + dcos[angle[player]]*3, ay[player] + dsin[angle[player]]*3, (angle[player] + rand() % 10 + 350) % 360, 1.4, 4, 4);
    }
    if (weapon == 9)
    {
        load[player] = 230;
        toinen_panos(ax[player] + dcos[angle[player]]*3, ay[player] + dsin[angle[player]]*3, angle[player], 1.1, 5, player);
    }
    if (weapon == 10)
    {
        load[player] = 320;
        toinen_panos(ax[player] + dcos[90]*4, ay[player] + dsin[90]*4, 90 + dcos[angle[player]]*30, asy[player] + 0.5, 6, player);
    }
    if (weapon == 11)
    {
        load[player] = 280;
        toinen_panos(ax[player] + dcos[angle[player]]*3, ay[player] + dsin[angle[player]]*3, angle[player], 1, 7, player);
    }
    if (weapon == 12)
    {
        load[player] = 250;
        toinen_panos(ax[player] + dcos[angle[player]]*3, ay[player] + dsin[angle[player]]*3, angle[player], 1, 8, player);
    }
    if (weapon == 13)
    {
        load[player] = 900;
        toinen_panos(ax[player] + dcos[angle[player]]*3, ay[player] + dsin[angle[player]]*3, angle[player], 1, 13, player);
    }
}

void aseta_pelipaletti()
{
    // tausta
    gfx->setPalette(255, 5, 1, 1);
    //alukset ja energiat
    gfx->setPalette(254, 63, 1, 1);
    gfx->setPalette(253, 1, 63, 1);
    gfx->setPalette(252, 1, 1, 63);
    gfx->setPalette(251, 63, 63, 1);
    //aseet (panokset)
    // normal
    gfx->setPalette(250, 44, 32, 53);

    // erikoiset
    gfx->setPalette(249, 35, 35, 35); //Diggers
    gfx->setPalette(248, 1, 1, 1); // käyttämätön
    gfx->setPalette(247, 1, 1, 1); // käyttämätön
    gfx->setPalette(246, 22, 35, 12); //Punisher
    gfx->setPalette(245, 45, 35, 45); //Missile
    gfx->setPalette(244, 15, 15, 40); //Bomb
    gfx->setPalette(243, 30, 35, 1); //Sprinkler
    gfx->setPalette(242, 1, 35, 55); //Shaker
    gfx->setPalette(241, 63, 1, 1); //Heat 1 pelaaja 0:n
    gfx->setPalette(240, 1, 63, 1); //Heat 2 pelaaja 1:n
    gfx->setPalette(239, 1, 1, 63); //Heat 3 pelaaja 2:n
    gfx->setPalette(238, 63, 63, 1); //Heat 4 pelaaja 3:n
    gfx->setPalette(237, 1, 1, 1); // käyttämätön
    gfx->setPalette(236, 1, 1, 1); // käyttämätön

    //bensa
    gfx->setPalette(218, 30, 25, 35);

    //rajahdys
    gfx->setPalette(217, 63, 63, 1);
    gfx->setPalette(216, 63, 45, 1);
    gfx->setPalette(215, 63, 20, 1);

    //savu
    gfx->setPalette(212, 45, 45, 30);

    //teksti
    gfx->setPalette(211, 63, 63, 63);
}

void nayta_tilanne()
{
    gfx->renderPCX("data/scores.pcx");
    gfx->setPalette(255, 255, 4, 4);
    gfx->setPalette(254, 4, 255, 4);
    gfx->setPalette(253, 4, 4, 255);
    gfx->setPalette(252, 255, 255, 4);
    gfx->setPalette(0, 170, 70, 150);


    put_only_letter(40, 50, 48 + tilanne[0], 255);
    if (pelaajat > 1) put_only_letter(265, 50, 48 + tilanne[1], 254);
    if (pelaajat > 2) put_only_letter(40, 150, 48 + tilanne[2], 253);
    if (pelaajat == 4)put_only_letter(265, 150, 48 + tilanne[3], 252);
    if (stages[laskuri] != 0 && laskuri < 5)
    {
        put_only_text(140, 50, "Round", 0);
        put_only_letter(156, 60, 48 + laskuri, 0);
    }
    else
    {
        put_only_text(140, 50, "Final", 0);
    }
    gfx->renderScreen();
    SDL_Delay(300);
    Uint32 key = waitForKey();
    gfx->clearPalette();
    if (key == SDL_SCANCODE_ESCAPE)
    {
        laskuri = 99;
    }
}

void ask_napit()
{
    const Uint8 *keybState = SDL_GetKeyboardState(NULL);

    for (int player = 0; player < pelaajat; player++)
    {
        if (ener[player] > 0)
        {
            if (keybState[leftKey[player]])
            {
                angle[player] -= 2;
                if (angle[player] < 0)angle[player] += 360;
            }
            if (keybState[rightKey[player]])
            {
                angle[player] += 2;
                if (angle[player] > 360)angle[player] -= 360;
            }
            if (keybState[up[player]])
            {
                if (asx[player] < dcos[angle[player]] * 1.5)asx[player] += 0.01;
                if (asx[player] > dcos[angle[player]] * 1.5)asx[player] -= 0.01;
                if (asy[player] < dsin[angle[player]] * 1.5)asy[player] += 0.01;
                if (asy[player] > dsin[angle[player]] * 1.5)asy[player] -= 0.008;
                if (bensa[player] > 0)
                {
                    bensa[player]--;
                }
                else
                {
                    if (rand() % 10 == 0)ener[player]--;
                    piirra_energiat();
                }
                tee_savu(player, angle[player], 20, 4);
            }

            // special
            if (keybState[spec[player]] == 1 && load[player] == 0)
            {
                erikoinen(player, special[player]);
            }

            // shoot
            if (keybState[shoot[player]] == 0)
            {
                shootRepeatBlock[player] = 0;
            }
            else if (shootRepeatBlock[player] == 0)
            {
                tee_panos(ax[player] + 2 * dcos[(angle[player] + 270) % 360], ay[player] + 2 * dsin[(angle[player] + 270) % 360], player, angle[player], 0);
                tee_panos(ax[player] + 2 * dcos[(angle[player] + 90) % 360], ay[player] + 2 * dsin[(angle[player] + 90) % 360], player, angle[player], 0);
                shootRepeatBlock[player] = 1;
            }
        }
    }
}

void game()
{
    char hip;
    unsigned int klo;
    unsigned char alusta;
    char loppu = 0, looppi;
    bensa[0] = 13000;
    bensa[1] = 13000;
    bensa[2] = 13000;
    bensa[3] = 13000;
    kesto = 500;
    klo = ticks();
    while (loppu == 0)
    {
        //Hidastus
        while (ticks() == klo) {
            SDL_Delay(10);
        }
        klo = ticks();
        for (hip = 0; hip < run_in_click; hip++)
        {
            // process events
            SDL_Event event;
            while (SDL_PollEvent(&event) == 1)
            {
                handle_global_events(event);
                switch (event.type)
                {
                case SDL_KEYDOWN:
                    if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
                    {
                        put_only_text(50, 15, "F10 to exit or ESC to return", 211);
                        gfx->renderScreen();
                        Uint32 key = 0;
                        do
                        {
                            key = waitForKey();
                        } while (key != SDL_SCANCODE_ESCAPE && key != SDL_SCANCODE_F10);
                        if (key == SDL_SCANCODE_F10)
                        {
                            loppu = 1;
                        }
                        gfx->fillRectangle(0, 0, 320, 25, 255);
                    }
                    break;
                }
            }

            // Hidastus

            for (int looppi = 0; looppi < lp; looppi++)
            {
                ask_napit();

                // lataa specialia
                for (int i = 0; i < pelaajat; i++)if (load[i] > 0)load[i]--;

                // special 2   (parannus)
                for (int i = 0; i < pelaajat; i++)
                    if (special[i] == 2 && load[i] == 0)
                    {
                        if (ener[i] < MAX_ENERGY - 1 && ener[i] > 0)ener[i]++;
                        load[i] = 250;
                        piirra_energiat();
                    }

                // lisää ja tarkista arvot

                for (int i = 0; i < pelaajat; i++)
                {
                    if (asx[i] > 0)asx[i] -= 0.0030;
                    else asx[i] += 0.0030;
                    if (asy[i] < 0)asy[i] += 0.0030;

                    if (ay[i] >= 200)
                    {
                        alusta = kuva_ala[(long) (ax[i] + asx[i])+(long) (ay[i] + asy[i] - 199)*320];
                        if (alusta >= 193 && alusta <= 196)
                        {
                            if (ener[i] < MAX_ENERGY && ener[i] > 0 && rep_speed[i] < 0)
                            {
                                ener[i]++;
                                rep_speed[i] = 20;
                                piirra_energiat();
                            }
                            else rep_speed[i]--;
                            if (ener[i] > 0)
                            {
                                if (kesto > 400)set_player_text(i, "Docking", 20);
                                if (bensa[i] < 13000)bensa[i] += 4;
                                int j = 0;
                                for (int h = 0; h < pelaajat; h++)
                                    if (h != i)
                                    {
                                        if (ener[h] > 0)j = 1;
                                    }
                                if (j == 0 && kesto > 400)
                                {
                                    kesto = 400;
                                    tilanne[i]++;
                                    set_player_text(i, "  Winner!", 1000);
                                }
                            }
                            angle[i] = 271;
                            asx[i] = 0;
                        }
                        alusta = kuva_ala[(long) (ax[i] + asx[i])+(long) (ay[i] + asy[i] - 200)*320];
                        if (alusta == 0 || alusta >= 197)
                        {
                            if (alusta == 0 || alusta > 197)asy[i] += 0.0030;
                            if (alusta == 197)
                            {
                                if (asx[i] > 0)asx[i] -= 0.0040;
                                else asx[i] += 0.0040;
                                if (asy[i]>-.3)asy[i] -= 0.0100;
                                else asy[i] += 0.0080;
                            }
                            ax[i] += asx[i];
                            ay[i] += asy[i];
                        }
                        else
                        {
                            if (ener[i] <= 0 && ener[i]>-100)
                            {
                                ener[i] = -100;
                                tee_rajays(ax[i], ay[i], 250, 2);
                            }
                            if (osuma[i] == 0)
                                if (asy[i] > 0.2 || asy[i]<-0.2 || asx[i] > 0.2 || asx[i]<-0.2)
                                {
                                    osuma[i] += 10;
                                    ener[i] -= (long) (abs(asx[i]*7) + std::abs(asy[i]*7));
                                    piirra_energiat();
                                }
                            asy[i] = 0;
                            asx[i] = 0;
                        }
                    }

                    if (ay[i] < 200)
                    {
                        alusta = kuva_yla[(long) (ax[i] + asx[i])+(long) (ay[i] + asy[i] + 1)*320];
                        if (alusta >= 193 && alusta <= 196)
                        {
                            if (ener[i] < MAX_ENERGY && ener[i] > 0 && rep_speed[i] < 0)
                            {
                                ener[i]++;
                                rep_speed[i] = 20;
                                piirra_energiat();
                            }
                            else rep_speed[i]--;
                            if (ener[i] > 0)
                            {
                                if (kesto > 400)set_player_text(i, "Docking", 20);
                                if (bensa[i] < 13000)bensa[i] += 4;
                                int j = 0;
                                for (int h = 0; h < pelaajat; h++)
                                    if (h != i)
                                    {
                                        if (ener[h] > 0)j = 1;
                                    }
                                if (j == 0 && kesto > 400)
                                {
                                    kesto = 400;
                                    tilanne[i]++;
                                    set_player_text(i, "  Winner!", 1000);
                                }
                            }
                            asx[i] = 0;
                            angle[i] = 271;
                        }
                        alusta = kuva_yla[(long) (ax[i] + asx[i])+(long) (ay[i] + asy[i])*320];
                        if (alusta == 0 || alusta >= 197)
                        {
                            if (alusta == 0 || alusta > 197)asy[i] += 0.0030;
                            if (alusta == 197)
                            {
                                if (asx[i] > 0)asx[i] -= 0.0040;
                                else asx[i] += 0.0040;
                                if (asy[i]>-.3)asy[i] -= 0.0100;
                                else asy[i] += 0.0080;
                            }
                            ax[i] += asx[i];
                            ay[i] += asy[i];
                        }
                        else
                        {
                            if (ener[i] <= 0 && ener[i]>-100)
                            {
                                ener[i] = -100;
                                tee_rajays(ax[i], ay[i], 250, 2);
                            }
                            if (osuma[i] == 0)
                                if (asy[i] > 0.2 || asy[i]<-0.2 || asx[i] > 0.2 || asx[i]<-0.2)
                                {
                                    osuma[i] += 10;
                                    ener[i] -= (long) (abs(asx[i]*7) + std::abs(asy[i]*7));
                                    piirra_energiat();
                                }
                            asy[i] = 0;
                            asx[i] = 0;
                        }
                    }
                    if (ax[i] < 5)
                    {
                        ax[i] = 5;
                        if (asx[i] < 0)asx[i] = 0.2;
                    }
                    if (ay[i] < 5)
                    {
                        ay[i] = 5;
                        if (asy[i] < 0)asy[i] = 0.2;
                    }
                    if (ax[i] > 315)
                    {
                        ax[i] = 315;
                        if (asx[i] > 0)asx[i] = -.2;
                    }
                    if (ay[i] > 395)
                    {
                        ay[i] = 395;
                        if (asy[i] > 0)asy[i] = -.2;
                    }
                }

                //savutus
                for (int i = 0; i < pelaajat; i++)if (ener[i]<(MAX_ENERGY >> 2) && ener[i] > 0)if (rand() % (ener[i])<(MAX_ENERGY / 30))tee_savu(i, 90, 50, 8);


            }

            // Putsaa ruudun alusta

            for (int h = 0; h < pelaajat; h++)
            {
                memset(&backbuffer[xkord[h] - 4 + (ykord[h] - 1)*320], 255, xleng[h] + 8);
                memset(&backbuffer[xkord[h] - 4 + (ykord[h] - 2)*320], 255, xleng[h] + 8);
                memset(&backbuffer[xkord[h] - 4 + (ykord[h] + yleng[h])*320], 255, xleng[h] + 8);
                memset(&backbuffer[xkord[h] - 4 + (ykord[h] + yleng[h] + 1)*320], 255, xleng[h] + 8);

                for (int i = 0; i < yleng[0]; i++)
                {
                    memset(&backbuffer[xkord[h] - 2 + (ykord[h] + i)*320], 255, 2);
                    memset(&backbuffer[xkord[h] + xleng[h]+(ykord[h] + i)*320], 255, 2);
                }
            }
            // nappien lukemiseen nopeutta
            ask_napit();
            // kohdista ruudut
            for (int h = 0; h < pelaajat; h++)
            {
                if ((int) ax[h] - xleng[h] / 2 > 1 && (int) ax[h] + xleng[h] / 2 < 319)xkohta[h] = (int) ax[h] - xleng[h] / 2;
                if ((int) ay[h] - yleng[h] / 2 > 1 && (int) ay[h] + yleng[h] / 2 < 399)ykohta[h] = (int) ay[h] - yleng[h] / 2;
            }

            // Tärähdys
            for (int h = 0; h < pelaajat; h++)
                if (osuma[h] > 1)
                {
                    if (ykohta[h] > 5 && ykohta[h] + yleng[h] < 395)ykohta[h] += rand() % 8 - 4;
                    if (xkohta[h] > 5 && xkohta[h] + xleng[h] < 315)xkohta[h] += rand() % 8 - 4;
                }

            // Pikku ruudut

            for (int h = 0; h < pelaajat; h++)
                for (int i = 0; i < yleng[0]; i++)
                {
                    if (ykohta[h] + i < 200)
                    {
                        memcpy(&backbuffer[xkord[h]+(ykord[h] + i)*320], &kuva_yla[xkohta[h]+(ykohta[h] + i)*320], xleng[h]);
                    }
                    else
                    {
                        memcpy(&backbuffer[xkord[h]+(ykord[h] + i)*320], &kuva_ala[xkohta[h]+(ykohta[h] + i - 199)*320], xleng[h]);
                    }
                }

            // piirra lennokit

            for (int i = 0; i < pelaajat; i++)
            {
                real_angr[i] = angle[i] - 140;
                real_angl[i] = angle[i] + 140;
                if (real_angl[i] > 360)real_angl[i] -= 360;
                if (real_angr[i] < 0) real_angr[i] += 360;
            }

            for (int i = 0; i < pelaajat; i++)
            {
                if (ener[i]>-100)
                {
                    gfx->drawLine((xkord[i]+(int) ax[i] - xkohta[i]+(int) (dcos[angle[i]]*4)), (ykord[i]+(int) ay[i] - ykohta[i]+(int) (dsin[angle[i]]*4)), (xkord[i]+((int) ax[i] - xkohta[i])+(int) (dcos[real_angl[i]]*4)), ((int) (dsin[real_angl[i]]*4)+(ykord[i]+(int) ay[i] - ykohta[i])), 254 - i);
                    gfx->drawLine((xkord[i]+(int) ax[i] - xkohta[i]+(int) (dcos[angle[i]]*4)), (ykord[i]+(int) ay[i] - ykohta[i]+(int) (dsin[angle[i]]*4)), (xkord[i]+((int) ax[i] - xkohta[i])+(int) (dcos[real_angr[i]]*4)), ((int) (dsin[real_angr[i]]*4)+(ykord[i]+(int) ay[i] - ykohta[i])), 254 - i);
                    gfx->drawLine((xkord[i]+(int) ax[i] - xkohta[i]+(int) (dcos[real_angl[i]]*4)), (ykord[i]+(int) ay[i] - ykohta[i]+(int) (dsin[real_angl[i]]*4)), (xkord[i]+((int) ax[i] - xkohta[i])+(int) (dcos[real_angr[i]]*4)), ((int) (dsin[real_angr[i]]*4)+(ykord[i]+(int) ay[i] - ykohta[i])), 254 - i);
                }
                for (int h = 0; h < pelaajat; h++)
                    if (h != i && ener[h]>-100 && ax[h] > xkohta[i] + 1 && ax[h] < xkohta[i] + xleng[i] - 1 && ay[h] > ykohta[i] + 1 && ay[h] < ykohta[i] + yleng[i] - 1 && special[h] != 5)
                    {
                        gfx->drawLine((xkord[i]+(int) ax[h] - xkohta[i]+(int) (dcos[angle[h]]*4)), (ykord[i]+(int) ay[h] - ykohta[i]+(int) (dsin[angle[h]]*4)), (xkord[i]+((int) ax[h] - xkohta[i])+(int) (dcos[real_angl[h]]*4)), ((int) (dsin[real_angl[h]]*4)+(ykord[i]+(int) ay[h] - ykohta[i])), 254 - h);
                        gfx->drawLine((xkord[i]+(int) ax[h] - xkohta[i]+(int) (dcos[angle[h]]*4)), (ykord[i]+(int) ay[h] - ykohta[i]+(int) (dsin[angle[h]]*4)), (xkord[i]+((int) ax[h] - xkohta[i])+(int) (dcos[real_angr[h]]*4)), ((int) (dsin[real_angr[h]]*4)+(ykord[i]+(int) ay[h] - ykohta[i])), 254 - h);
                        gfx->drawLine((xkord[i]+(int) ax[h] - xkohta[i]+(int) (dcos[real_angl[h]]*4)), (ykord[i]+(int) ay[h] - ykohta[i]+(int) (dsin[real_angl[h]]*4)), (xkord[i]+((int) ax[h] - xkohta[i])+(int) (dcos[real_angr[h]]*4)), ((int) (dsin[real_angr[h]]*4)+(ykord[i]+(int) ay[h] - ykohta[i])), 254 - h);
                    }
            }
            for (looppi = 0; looppi < lp; looppi++)
            {
                // Liikuta panoksia
                for (int i = 0; i < MAX_SMOKE; i++)if (sav[i] > 0)
                    {
                        liikuta_savu(i, looppi);
                    }
                for (int i = 0; i < MAX_AMMO; i++)if (px[i] > 3)
                    {
                        liikuta_panos(i, looppi);
                        psy[i] += 0.003;
                    }
                for (int i = 0; i < MAX_SPECIAL; i++)if (ex[i] > 3)
                    {
                        liikuta_erikoinen(i, looppi);
                        pey[i] += 0.003;
                    }
            }

            for (int i = 0; i < 40; i++)if (raj_time[i] > 0)
                {
                    raj_time[i]--;
                    if (raj_time[i] == 0)
                        for (int h = 0; h < 12; h++)
                            for (int d = 0; d < 12; d++)
                            {
                                if (rajahdys[d][h] != 0)
                                {
                                    if (raj_y[i] - 6 + h < 200)
                                    {
                                        if (raj_y[i] - 6 + h > 1 && raj_x[i] - 6 + d > 1 && raj_x[i] - 6 + d < 319)
                                            if ((unsigned char) (kuva_yla [raj_x[i] - 6 + d + 320 * (raj_y[i] + h - 6)]) >= 215)
                                                kuva_yla [raj_x[i] - 6 + d + 320 * (raj_y[i] + h - 6)] = 0;
                                    }
                                    else
                                        if (raj_y[i] - 6 + h < 400 && raj_x[i] - 6 + d > 1 && raj_x[i] - 6 + d < 319)
                                        if ((unsigned char) (kuva_ala[raj_x[i] - 6 + d + 320 * (raj_y[i] + h - 206)]) >= 215)
                                            kuva_ala[raj_x[i] - 6 + d + 320 * (raj_y[i] + h - 206)] = 0;
                                }
                            }
                }

            // Draw overlay text
            for (int h = 0; h < pelaajat; h++)
            {
                if (pituus[h] > 0)
                {
                    pituus[h]--;
                    put_only_text(xkord[h] + 5, ykord[h] + 5, viesti[h], 254 - h);
                }
            }
            piirra_bensa();
            gfx->renderScreen();

            for (int i = 0; i < pelaajat; i++)if (osuma[i] > 0)osuma[i]--;

            int h = 1;
            for (int i = 0; i < pelaajat; i++)
            {
                if (ener[i] > 0) h = 0;
            }
            if (h == 1 && kesto > 400)
            {
                kesto = 400;
                for (int h = 0; h < pelaajat; h++)
                {
                    set_player_text(h, "DRAW", 1000);
                }
            }

            if (kesto < 401)kesto--;
            if (kesto < 2)loppu = 1;
        }
    }
}

int findStages()
{
    struct _finddata_t fileinfo;
    int handle = _findfirst("stages/*.pcx", &fileinfo);
    if (handle == -1)
    {
        return 0;
    }
    int err = 0;
    int i = 1;
    while (err == 0)
    {
        debugTrace("%2d %s", i++, &fileinfo.name);
        stageNames.push_back(fileinfo.name);
        err = _findnext(handle, &fileinfo);
    }
    return stageNames.size();
}

void initialize()
{
    debugTrace("Starting BOOMBOOM ver 1.00...");
    std::srand(std::time(0));

    // Load configuration
    debugTrace("Loading keys...");
    int keyFile;
    if ((keyFile = _open("KEYS.BB", O_RDWR)) != -1)
    {
        // TODO:
        //_read(keyFile, up, sizeof (up));
        //_read(keyFile, leftKey, sizeof (leftKey));
        //_read(keyFile, rightKey, sizeof (rightKey));
        //_read(keyFile, shoot, sizeof (shoot));
        //_read(keyFile, spec, sizeof (spec));
        for (int i = 0; i < 4; i++)
        {
            debugTrace("[%d] %d %d %d - %d %d", i, up[i], leftKey[i], rightKey[i], shoot[i], spec[i]);
        }
        _close(keyFile);
    }
    else
    {
        debugTrace("Can't Find keyboard setup file. Using standard keyboard setup");
    }

    // Finding stages
    debugTrace("Finding stages...");
    if (findStages() == 0)
    {
        error("Can't find any stages!! Read Boomboom.txt how to create your own stages.");
    }

    // Initializing graphics
    debugTrace("Initializing graphics...");

    gfx = new Gfx();
    if (!gfx->init())
    {
        error("Graphics initialization error!");
    }
    backbuffer = gfx->screenPixels();

    for (int j = 0; j < 5; j++)
    {
        stages[j] = rand() % stageCount() + 1;
    }

    // calculate sin and cos
    for (int j = 0; j < 361; j++)
    {
        dcos[j] = cosdeg(j);
        dsin[j] = sindeg(j);
    }
}

void mainmenu()
{
    // render menu background
    gfx->renderPCX("data/boomboom.pcx");
    // Start menu
    const int MENU_NEW_GAME = 255;
    const int MENU_LEVELS = 254;
    const int MENU_OPTIONS = 253;
    const int MENU_QUIT = 252;
    int valikko = MENU_QUIT;
    while (valikko != MENU_NEW_GAME)
    {
        gfx->fillRectangle(114, 67, 199, 122, 206);
        gfx->putText(125, 68, "New game", MENU_NEW_GAME, 206);
        gfx->putText(133, 83, "Levels", MENU_LEVELS, 206);
        gfx->putText(129, 98, "Options", MENU_OPTIONS, 206);
        gfx->putText(141, 113, "Quit", MENU_QUIT, 206);
        gfx->setPalette(MENU_NEW_GAME, 63, 1, 1);
        gfx->setPalette(MENU_LEVELS, 5, 7, 2);
        gfx->setPalette(MENU_OPTIONS, 5, 7, 2);
        gfx->setPalette(MENU_QUIT, 5, 7, 2);
        gfx->renderScreen();
        valikko = MENU_NEW_GAME;
        int i = 0;
        while (i == 0) {
            SDL_Event event;
            while (SDL_PollEvent(&event)) 
            {
                handle_global_events(event);
                switch (event.type)
                {
                case SDL_KEYDOWN:
                    if (event.key.keysym.scancode == SDL_SCANCODE_UP && valikko < MENU_NEW_GAME)
                    {
                        gfx->setPalette(valikko, 5, 7, 2);
                        valikko++;
                        gfx->setPalette(valikko, 63, 1, 1);
                        gfx->renderScreen();
                    }
                    if (event.key.keysym.scancode == SDL_SCANCODE_DOWN && valikko > MENU_QUIT)
                    {
                        gfx->setPalette(valikko, 5, 7, 2);
                        valikko--;
                        gfx->setPalette(valikko, 63, 1, 1);
                        gfx->renderScreen();
                    }
                    if (event.key.keysym.scancode == SDL_SCANCODE_RETURN)
                    {
                        i = 1;
                    }
                    break;
                }
            }
            SDL_Delay(50);
        }

        if (valikko == MENU_QUIT)
        {
            delete gfx;
            exit(0);
        }

        if (valikko == MENU_LEVELS)
        {
            select_stage();
        }

        if (valikko == MENU_OPTIONS)
        {
            options();
        }
    }

}

void mainGameLoop()
{
    // start game
    gfx->clearPalette();

    laskuri = 0;
    tilanne[0] = 0;
    tilanne[1] = 0;
    tilanne[2] = 0;
    tilanne[3] = 0;

    while (stages[laskuri] != 0 && laskuri < 5)
    {
        clear_data();

        // Load level
        std::string fullName = stageNameFile(stages[laskuri]);
        SDL_Surface* level = gfx->loadPCX(fullName.c_str());
        if (level->w != 320)error("Stage width must be 320 pixel");
        if (level->h != 400)error("Stage depth must be 400 pixel");
        if (level->format->BytesPerPixel != 1)error("Stage hasn't got 256 colors!");

        kuva_yla = (Uint8*)level->pixels;
        kuva_ala = (Uint8*)level->pixels + (level->w * level->h) / 2;
        gfx->clearPalette();
        gfx->clearScreen();
        gfx->renderScreen();

        gfx->copyPalette(level->format->palette);
        piirra_energiat();
        aseta_pelipaletti();
        gfx->renderScreen();

        // Arvo aseet
        if (cheat == 0)
        {
            special[0] = rand() % 13;
            special[1] = rand() % 13;
            special[2] = rand() % 13;
            special[3] = rand() % 13;
        }
        if (cheat == 1)
        {
            special[0] = rand() % 14;
            special[1] = rand() % 14;
            special[2] = rand() % 14;
            special[3] = rand() % 14;
        }

        // Show special weapons
        for (int h = 0; h < pelaajat; h++)
        {
            set_player_text(h, aseet[special[h]], 700);
        }

        // arvo aloitus paikka
        for (int h = 0, i = 0; h < pelaajat; h++)
        {
            do
            {
                i = 0;
                ax[h] = rand() % 310 + 5;
                ay[h] = rand() % 390 + 5;
                if ((int)(ay[h] >= 200))
                {
                    if ((unsigned char)kuva_ala[(int)(ax[h]) + (int)(ay[h] - 200) * 320] != 0 && (unsigned char)kuva_ala[(int)(ax[h]) + (int)(ay[h] - 200) * 320] < 197)i = 1;
                }
                else if ((unsigned char)kuva_yla[(int)(ax[h]) + (int)(ay[h]) * 320] != 0 && (unsigned char)kuva_yla[(int)(ax[h]) + (int)(ay[h]) * 320] < 197)i = 1;
                if (ax[h] < xleng[h])xkohta[h] = 1;
                if (ay[h] < yleng[h])ykohta[h] = 1;
                if (ax[h] > 320 - xleng[h])xkohta[h] = 319 - xleng[h];
                if (ay[h] > 400 - yleng[h])ykohta[h] = 399 - yleng[h];

            } while (i == 1);
        }


        // ITSE PELI !!!!
        for (int i = 0; i < pelaajat; i++)
        {
            if ((int)ax[i] - xleng[i] / 2 > 1 && (int)ax[i] + xleng[i] / 2 < 319)xkohta[i] = (int)ax[i] - xleng[i] / 2;
            if ((int)ay[i] - yleng[i] / 2 > 1 && (int)ay[i] + yleng[i] / 2 < 399)ykohta[i] = (int)ay[i] - yleng[i] / 2;

            if ((int)ax[i] - xleng[i] / 2 < 0)xkohta[i] = 1;
            if ((int)ax[i] + xleng[i] / 2 > 319)xkohta[i] = 319 - xleng[i];
            if ((int)ay[i] - yleng[i] / 2 < 0)ykohta[i] = 1;
            if ((int)ay[i] + yleng[i] / 2 > 399)ykohta[i] = 399 - yleng[i];
        }
        game();
        laskuri++;
        gfx->clearPalette();
        nayta_tilanne();
    }
}

/*
 *
 */
int main(int argc, char** argv)
{
    // cheat codes
    for (int i = 0; i < argc; i++)
    {
        if (strcmp(argv[i], "samy") == 0)
        {
            debugTrace("Cheat mode enabled!");
            cheat = 1;
        }
    }

    initialize();

    while(1)
    {
        mainmenu();
        mainGameLoop();
    }
}
