#ifndef PCX_H
#define	PCX_H

typedef struct
{
    uint8_t manufacturer;
    uint8_t version;
    uint8_t encoding;
    uint8_t bits_per_pixel;
    uint16_t xmin, ymin;
    uint16_t xmax, ymax;
    uint16_t hres;
    uint16_t vres;
    uint8_t palette[48];
    uint8_t reserved;
    uint8_t colour_planes;
    uint16_t bytes_per_line;
    uint16_t palette_type;
    uint8_t filler[58];
} PCXHEAD;

#endif	/* PCX_H */

