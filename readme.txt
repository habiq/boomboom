﻿                               BOOMBOOM 1.00

-----------------------------------------------------------------------------
Controls...
-----------------------------------------------------------------------------
Here are the DEFAULT buttons for the game.
            
            Player 1         Player 2      Player 3    Player 4
Foward        [W]          [Num pad 5]       [P]         [Y]
Left          [A]          [Num pad 1]       [L]         [G]
Right         [D]          [Num pad 3]       [Ä]         [J]
Shoot         [§]          [Num pad 4]       [O]         [T]
Special       [S]          [Num pad 2]       [Ö]         [H]

In this windows version these keys can't be changed. Sorry!

To select stages randomly use [R] or numbers [1]-[5].

[F12] to toggle fullscreen mode

-----------------------------------------------------------------------------
Special weapons...
-----------------------------------------------------------------------------
There are special weapons witch are used in game. And you get them randomly 
before every stage.
        Here they are:

Teleport            : teleports you to random place

Multi shotgun       : Shots bullets around you. 

Repairer            : Repairs you ship little by little.

Shotgun             : Shots some bullets foward.

Rearshotgun         : Shots some bullets backward.

Invisibility        : You are invisible for other players.

Diggers             : Bullets that "eats ground".

Heat Hunters        : Missile that Finds nearest craft.

Missile             : Explosive missile that flights straight foward.

Bomb                : Large back of explotion that you can drop from your 
                      craft.

Sprinkler           : Missile that shoots bullets all around.

Shaker              : Virus that shakes opponents screen.

(JUDGER!)           : Very DESTRUCTIVE secret weapon.

-----------------------------------------------------------------------------
To create your own stages...
-----------------------------------------------------------------------------

To create stages to Boomboom you only need a painting program that
can save pictures in PCX format. stage is 320 pixels width and 400 pixels
high and uses 256 colors.

here is the color table:
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
color : 0       -> Air where ships fly on. (you can use your own color!)
colors: 1-192   -> Colors that you can use drawing the stage. (walls)
colors: 193-196 -> Docking base.
colors: 197     -> Water. (you can use your own color!)
colors: 198-209 -> Colors where you can fly on. (like air)
colors: 210-256 -> Colors that are reserved for the game.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
I have used PSP in drawing stages and I think it's good enough ;)

-----------------------------------------------------------------------------
Here are some words before the end...
-----------------------------------------------------------------------------
I'd like to thank these guys who helped me to create this game:

******************
Jari-Pekka Honkatukia                : Beta testing
Tero Keskinen                        : Beta testing
Esa Kekäläinen                       : Beta testing
Timo Saarnio                         : Beta testing
Markus Makkonen                      : Beta testing 
Pasi Piitulainen                     : Beta testing   
Marko Kovanen                        : Beta testing
******************

If you have something to say about the game or if you have created good
levels you should send them to me and i will (if they are good enough)
use them in my later versions. My E-mail address is: potaatti@hotmail.com. 

This version of BoomBoom have much of bugs that I am not able to fix
but maybe I am able to do that in the next release of BoomBoom.
Goals for next release:
        - Sounds (GUS)
        - Bigger stages
        - More weapons
        - Fix bugs (of course)

----                                                       ----
this game is freeware because I think that there is nothing new
in this game and there are much better games out there (I like
Wings is the best of them). And didn't do this game for money
I did it for experience and for fun.
----                                                       ----

                            Enjoy !!!
                                                

---------------------------------------------------------------
Additions 2015
---------------------------------------------------------------
												
Text above is the original readme file which was included with
the game. There's lots of typos and errors but I wanted to keep
it included as such.

Changes since original release:
* Quick and Dirty conversion from DOS age into modern PC age (using SDL)
* Published the source codes with MIT license (https://bitbucket.org/habiq)


